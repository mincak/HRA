#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "Rest/Request.h"
#include "Rest/Response.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_get_clicked();
    void on_post_clicked();
    void showResponse(Response *response);

    void on_post2_clicked();

private:
    Ui::MainWindow *ui;
    Request *_request;
};

#endif // MAINWINDOW_H
