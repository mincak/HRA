#ifndef RESPONSE_H
#define RESPONSE_H

#include <QObject>
#include <QNetworkReply>

class Response : public QObject
{
    Q_OBJECT
public:
    explicit Response(QNetworkReply *reply, QObject *parent = nullptr);
    bool hasError();
    QString error();
    QByteArray data();

signals:

public slots:

private:
    QNetworkReply *_reply;
    QByteArray _data;
};

#endif // RESPONSE_H
