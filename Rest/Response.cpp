#include "Response.h"
#include <QDebug>
#include <QPixmap>

Response::Response(QNetworkReply *reply, QObject *parent) : QObject(parent)
{
    _reply = reply;

    if(!hasError())
        _data = _reply->readAll();
}

bool Response::hasError()
{
    QNetworkReply::NetworkError errorType = _reply->error();
    qDebug() << errorType;
    return (errorType != QNetworkReply::NoError);
}

QString Response::error()
{
    return hasError() ? _reply->errorString() : "No error";
}

QByteArray Response::data()
{
    return _data;
}
