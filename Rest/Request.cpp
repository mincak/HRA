#include "Request.h"
#include <QDebug>

Request::Request(QObject *parent) : QObject(parent)
{
    _mgr = new QNetworkAccessManager(this);

    connect(_mgr, SIGNAL(finished(QNetworkReply*)), SLOT(requestFinished(QNetworkReply*)));
}

void Request::run(const QString &type, QString url, const QMap<QString, QString> &inputMap)
{
    //const QString &input("{\"x\":1,\"y\":1}");
    const QString &input = "";
    QNetworkRequest request;

    // Hlavicka
    QString concatenated = "C++_6:Lubos";
    QByteArray data = concatenated.toLocal8Bit().toBase64();
    QString headerData = "Basic " + data;
    request.setRawHeader("Authorization", headerData.toLocal8Bit());
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    qDebug() << "Authorization:" << headerData;

    //qDebug() << "Koordinaty:" << input;

    if(type == "GET") {
        if(!input.isEmpty())
            url.append(QString("?%1").arg(input));
        qDebug() << "Request:" << type << url;
        request.setUrl(url);
        type=="GET" ? _mgr->get(request) : _mgr->head(request);
    }
    else if(type == "POST") {
        request.setUrl(url);
        _mgr->post(request, input.toLatin1());
    }
}

void Request::requestFinished(QNetworkReply *reply)
{
    if(_response)
        delete _response;
    _response = new Response(reply, this);

    emit response(_response);
}

