#include "MainWindow.h"
#include "ui_MainWindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    _request = new Request(this);
    connect(_request, SIGNAL(response(Response*)), SLOT(showResponse(Response*)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_get_clicked()
{
    _request->run("GET", "http://bciapi.eu-west-1.elasticbeanstalk.com/game/board");
}

void MainWindow::on_post_clicked()
{
    QMap<QString, QString> inputMap;
    _request->run("POST", "http://bciapi.eu-west-1.elasticbeanstalk.com/game/players/join", inputMap);
}

void MainWindow::on_post2_clicked()
{
    QMap<QString, QString> inputMap;
    inputMap.insert("x", "1");
    inputMap.insert("y", "1");

    _request->run("POST", "http://bciapi.eu-west-1.elasticbeanstalk.com/game/bewegen", inputMap);
}

void MainWindow::showResponse(Response *response)
{
    if(response->hasError())
        ui->body->setPlainText(response->error());
    else
        ui->body->setPlainText(response->data());
}

